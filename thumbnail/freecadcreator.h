/*
 *  SPDX-License-Identifier: LGPL-3-or-later
 *  SPDX-FileCopyrightText: 2022 Thomas Wilczek <dev@thomas-wilczek.info>
 */

#pragma once

#include <KIO/ThumbnailCreator>

class FreeCADCreator : public KIO::ThumbnailCreator
{
public:
    FreeCADCreator(QObject *parent, const QVariantList &args);
    ~FreeCADCreator() override;

    KIO::ThumbnailResult create(const KIO::ThumbnailRequest &request) override;
};
