/*
 *  SPDX-License-Identifier: LGPL-3-or-later
 *  SPDX-FileCopyrightText: 2022 Thomas Wilczek <dev@thomas-wilczek.info>
 */

#include "freecadcreator.h"

#include <KPluginFactory>
#include <kzip.h>

#include <QIODevice>
#include <QImage>
#include <QStringLiteral>
#include <qnamespace.h>

#include <memory>

K_PLUGIN_CLASS_WITH_JSON(FreeCADCreator, "freecadthumbnail.json")

FreeCADCreator::FreeCADCreator(QObject *parent, const QVariantList &args)
    : KIO::ThumbnailCreator(parent, args)
{
}

FreeCADCreator::~FreeCADCreator() = default;

KIO::ThumbnailResult FreeCADCreator::create(const KIO::ThumbnailRequest &request)
{
    KZip zip(request.url().toLocalFile());

    if (!zip.open(QIODevice::ReadOnly)) {
        return KIO::ThumbnailResult::fail();
    }

    const auto *entry = zip.directory()->file(QStringLiteral("thumbnails/Thumbnail.png"));

    if (!entry) {
        return KIO::ThumbnailResult::fail();
    }

    std::unique_ptr<QIODevice> fileDevice{entry->createDevice()};
    QImage img;

    if (img.load(fileDevice.get(), "PNG")) {
        if (img.width() < request.targetSize().width() || img.height() < request.targetSize().height()) {
            img = img.scaled(request.targetSize(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
        }

        return KIO::ThumbnailResult::pass(img);
    }

    return KIO::ThumbnailResult::fail();
}

#include "freecadcreator.moc"
