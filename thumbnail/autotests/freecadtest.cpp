/*
    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
    SPDX-FileCopyrightText: 2022 Thomas Wilczek <dev@thomas-wilczek.info>
*/

#include <QSignalSpy>
#include <QStandardPaths>
#include <QTest>

#include <KIO/PreviewJob>

class FreeCADTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:

    void testFreeCAD_data()
    {
        QTest::addColumn<QString>("inputFile");
        QTest::addColumn<QString>("expectedThumbnail");
        QTest::addColumn<qreal>("dpr");

        QTest::addRow("KDE Logo") << "thumbtest.FCStd"
                                  << "freecadthumb.​png" << 1.0;
    }

    void testFreeCAD()
    {
        QFETCH(QString, inputFile);
        QFETCH(QString, expectedThumbnail);
        QFETCH(qreal, dpr);

        QStandardPaths::setTestModeEnabled(true);
        qputenv("KIOSLAVE_ENABLE_TESTMODE", "1"); // ensure the worker call QStandardPaths::setTestModeEnabled too

        // wipe thumbnail cache so we always start clean
        QDir cacheDir(QStandardPaths::writableLocation(QStandardPaths::GenericCacheLocation));
        cacheDir.removeRecursively();

        QString path = QFINDTESTDATA("data/" + inputFile);

        KFileItemList items;
        items.append(KFileItem(QUrl::fromLocalFile(path)));

        QStringList enabledPlugins{"freecadthumbnail"};
        auto job = KIO::filePreview(items, QSize(256, 256), &enabledPlugins);
        job->setDevicePixelRatio(dpr);

        connect(job, &KIO::PreviewJob::gotPreview, this, [path, expectedThumbnail, dpr](const KFileItem &item, const QPixmap &preview) {
            QCOMPARE(item.url(), QUrl::fromLocalFile(path));

            QImage expectedImage;
            expectedImage.load(QFINDTESTDATA("data/" + expectedThumbnail));
            expectedImage.setDevicePixelRatio(dpr);

            QCOMPARE(preview.toImage(), expectedImage);
        });

        QSignalSpy gotPreviewSpy(job, &KIO::PreviewJob::gotPreview);
        gotPreviewSpy.wait();
    }
};

QTEST_MAIN(FreeCADTest)

#include "freecadtest.moc"
