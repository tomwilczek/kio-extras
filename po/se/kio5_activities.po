# Translation of kio5_activities to Northern Sami
#
# Børre Gaup <boerre@skolelinux.no>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-15 00:47+0000\n"
"PO-Revision-Date: 2011-10-26 01:37+0200\n"
"Last-Translator: Børre Gaup <boerre@skolelinux.no>\n"
"Language-Team: Northern Sami <l10n-no@lister.huftis.org>\n"
"Language: se\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: fileitemplugin/FileItemLinkingPlugin.cpp:96 KioActivities.cpp:179
#, kde-format
msgid "Activities"
msgstr ""

#: fileitemplugin/FileItemLinkingPlugin.cpp:99
#, kde-format
msgid "Loading..."
msgstr ""

#: fileitemplugin/FileItemLinkingPlugin.cpp:118
#, kde-format
msgid "The Activity Manager is not running"
msgstr ""

#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:53
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:125
#, kde-format
msgid "Link to the current activity"
msgstr ""

#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:56
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:130
#, kde-format
msgid "Unlink from the current activity"
msgstr ""

#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:59
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:134
#, kde-format
msgid "Link to:"
msgstr ""

#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:64
#: fileitemplugin/FileItemLinkingPluginActionLoader.cpp:142
#, kde-format
msgid "Unlink from:"
msgstr ""

#: KioActivities.cpp:110
#, kde-format
msgid "Current activity"
msgstr ""

#: KioActivities.cpp:111 KioActivitiesApi.cpp:85
#, kde-format
msgid "Activity"
msgstr ""
