# translation of kio_man.po to greek
# Copyright (C) 2000,2003, 2005, 2007, 2008 Free Software Foundation, Inc.
#
# Dimitris Kamenopoulos <el97146@mail.ntua.gr>, 2000-2002.
# Stergios Dramis <sdramis@egnatia.ee.auth.gr>, 2003.
# Spiros Georgaras <sngeorgaras@otenet.gr>, 2005, 2007.
# Toussis Manolis <manolis@koppermind.homelinux.org>, 2007, 2008.
# Spiros Georgaras <sng@hellug.gr>, 2008.
# Dimitrios Glentadakis <dglent@gmail.com>, 2012.
# Stelios <sstavra@gmail.com>, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kio_man\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-10 00:45+0000\n"
"PO-Revision-Date: 2021-09-03 10:25+0300\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kio_man.cpp:451
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No man page matching <resource>%1</resource> could be found.<nl/><nl/>Check "
"that you have not mistyped the name of the page, and note that man page "
"names are case sensitive.<nl/><nl/>If the name is correct, then you may need "
"to extend the search path for man pages, either using the <envar>MANPATH</"
"envar> environment variable or a configuration file in the <filename>/etc</"
"filename> directory."
msgstr ""
"Δε βρέθηκε σελίδα man που να ταιριάζει με το <resource>%1</resource>.<nl/"
"><nl/>Ελέγξτε ότι δεν έχετε γράψει λάθος το όνομα της σελίδας και σημειώστε "
"ότι οι σελίδες εγχειριδίου κάνουν διάκριση μεταξύ πεζών και κεφαλαίων "
"χαρακτήρων.<nl/><nl/>Αν το όνομα είναι σωστό, τότε ίσως θα πρέπει να "
"επεκτείνετε τη διαδρομή αναζήτησης για τις σελίδες εγχειριδίου μέσω της "
"μεταβλητής περιβάλλοντος <envar>MANPATH</envar> ή ένα αρχείο διαμόρφωσης "
"στον κατάλογο <filename>/etc</filename>."

#: kio_man.cpp:569
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The specified man page references another page <filename>%1</filename>,<nl/"
">but the referenced page <filename>%2</filename> could not be found."
msgstr ""
"Η δοσμένη σελίδα εγχειριδίου αναφέρεται σε άλλη σελίδα <filename>%1</"
"filename>,<nl/>αλλά η αναφερόμενη σελίδα <filename>%2</filename> δε βρέθηκε."

#: kio_man.cpp:586
#, kde-kuit-format
msgctxt "@info"
msgid "The man page <filename>%1</filename> could not be read."
msgstr ""
"Αδυναμία ανάγνωσης της σελίδας του εγχειριδίου <filename>%1</filename>."

#: kio_man.cpp:595
#, kde-kuit-format
msgctxt "@info"
msgid "The man page <filename>%1</filename> could not be converted."
msgstr ""
"Αδυναμία μετατροπής της σελίδας του εγχειριδίου <filename>%1</filename>."

#: kio_man.cpp:660
#, kde-format
msgid "Manual Page Viewer Error"
msgstr "Σφάλμα του προβολέα σελίδων εγχειριδίου"

#: kio_man.cpp:674
#, kde-format
msgid "There is more than one matching man page:"
msgstr "Ταιριάζουν πάνω από μία σελίδες εγχειριδίου:"

#: kio_man.cpp:674
#, kde-format
msgid "Multiple Manual Pages"
msgstr "Πολλές σελίδες εγχειριδίου"

#: kio_man.cpp:686
#, kde-format
msgid ""
"Note: if you read a man page in your language, be aware it can contain some "
"mistakes or be obsolete. In case of doubt, you should have a look at the "
"English version."
msgstr ""
"Σημείωση: αν διαβάζετε μία σελίδα man στη γλώσσα σας, πρέπει να έχετε υπόψη "
"σας ότι ίσως περιέχει σφάλματα ή είναι ξεπερασμένη. Ίσως θα πρέπει να ρίξετε "
"μια ματιά και στην Αγγλική έκδοσή της."

#: kio_man.cpp:758
#, kde-format
msgid "Header Files"
msgstr "Header αρχεία"

#: kio_man.cpp:759
#, kde-format
msgid "Header Files (POSIX)"
msgstr "Header αρχεία (POSIX)"

#: kio_man.cpp:760
#, kde-format
msgid "User Commands"
msgstr "Εντολές χρήστη"

#: kio_man.cpp:761
#, kde-format
msgid "User Commands (POSIX)"
msgstr "Εντολές χρήστη (POSIX)"

#: kio_man.cpp:762
#, kde-format
msgid "System Calls"
msgstr "Κλήσεις συστήματος"

#: kio_man.cpp:763
#, kde-format
msgid "Subroutines"
msgstr "Υπορουτίνες"

#: kio_man.cpp:765
#, kde-format
msgid "Perl Modules"
msgstr "Αρθρώματα perl"

#: kio_man.cpp:766
#, kde-format
msgid "Network Functions"
msgstr "Λειτουργίες δικτύου"

#: kio_man.cpp:767
#, kde-format
msgid "Devices"
msgstr "Συσκευές"

#: kio_man.cpp:768
#, kde-format
msgid "File Formats"
msgstr "Μορφές αρχείων"

#: kio_man.cpp:769
#, kde-format
msgid "Games"
msgstr "Παιχνίδια"

#: kio_man.cpp:770
#, kde-format
msgid "Miscellaneous"
msgstr "Διάφορα"

#: kio_man.cpp:771
#, kde-format
msgid "System Administration"
msgstr "Διαχείριση συστήματος"

#: kio_man.cpp:772
#, kde-format
msgid "Kernel"
msgstr "Πυρήνας"

#: kio_man.cpp:773
#, kde-format
msgid "Local Documentation"
msgstr "Τοπική τεκμηρίωση"

#: kio_man.cpp:775
#, kde-format
msgid "New"
msgstr "Νέα"

#: kio_man.cpp:805
#, kde-format
msgid "Main Manual Page Index"
msgstr "Ευρετήριο κύριας σελίδας εγχειριδίου"

#: kio_man.cpp:830
#, kde-format
msgid "Section %1"
msgstr "Τμήμα %1"

#: kio_man.cpp:1115
#, kde-format
msgid "Index for section %1: %2"
msgstr "Ευρετήριο για την ενότητα %1: %2"

#: kio_man.cpp:1115
#, kde-format
msgid "Manual Page Index"
msgstr "Ευρετήριο σελίδας εγχειριδίου"

#: kio_man.cpp:1127
#, kde-format
msgid "Generating Index"
msgstr "Δημιουργία ευρετηρίου"

#: kio_man.cpp:1405
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Could not find the <command>%1</command> program on your system. Please "
"install it if necessary, and ensure that it can be found using the "
"environment variable <envar>PATH</envar>."
msgstr ""
"Αδυναμία εύρεσης του προγράμματος <command>%1</command> στο σύστημά σας. "
"Eγκαταστήστε το αν είναι απαραίτητο, και βεβαιωθείτε ότι μπορεί να βρεθεί με "
"χρήση της μεταβλητής περιβάλλοντος <envar>PATH</envar>."

#~ msgid "Open of %1 failed."
#~ msgstr "Το άνοιγμα του %1 απέτυχε."

#~ msgid "Man output"
#~ msgstr "Έξοδος man"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Τούσης Μανώλης"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "manolis@koppermind.homelinux.org"

#~ msgid "KMan"
#~ msgstr "KMan"
